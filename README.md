# SudokuResolver

Sudoku resolver challenge. Let's find different ways to resolve a sudoku programmatically, then optimize and benchmark the diverse solutions.

## Introduction
The purpose of this challenge is to develop different solutions in PHP by using OOP.

## Resources
- Some sudokus with their solutions are provided in `data.json`. 
- `resolvers/SimpleResolver.php` example of class. Solutions classes must be stored in the folder `resolvers`. They need to include a `resolve()` static method that contains the logic to resolve a sudoku. The first parameter `$problem` will receive every sudokus stored in `data.json` and must return the solution under the same format (see `solution` keys in `data.json` if necessary).
- `benchmark.php`: a simple way to benchmark all resolvers found in the `resolvers` folder.

## Goal
1. Train unit and feature testing
1. Learn to solve problems
1. Try to optimize the duration of our solutions
1. Train OOP in PHP
1. And have fun !

## How to run the benchmark ?
```bash
php -f benchmark.php
```

```bash
SUDOKU Resolver challenge
-------------------------

Here is the benchmark result:
1. SimpleResolver - 3.00016s 
2. RandomResolver - 2.00017s 
```

## Licence
SudokuResolver is released under the [`AGPL-3.0-or-later`](LICENSE).

	SudokuResolver - A sudoku resolver challenge in PHP
	Copyright (C) 2022-present Samuel Roland