<?php

//Import the different classes you have created

$times = [];
$sudokus = json_decode(file_get_contents("data.json"))->sudokus;
$resolvers = scandir("resolvers");
$resolvers = array_filter($resolvers, function ($file) {
	return !in_array($file, ['.', '..']);
});
$resolvers = array_map(function ($file) {
	return substr($file, 0, strpos($file, "."));
}, $resolvers);

//Run benchmarks on each resolvers with each sudokus
foreach ($resolvers as $resolver) {
	include "resolvers/$resolver.php";

	$stats[$resolver] = [
		"resolver" => $resolver,
		'start' => microtime(true),
		'end' => null,
		"result" => null,
		"correct" => null,
	];

	foreach ($sudokus as $key => $sudoku) {

		$response = $resolver::resolve($sudoku->problem);
		$correct = $response == $sudoku->solution;

		$stats[$resolver]['result'][$key] = $response;
		$stats[$resolver]['correct'][$key] = $correct;
	}

	$stats[$resolver]['end'] = microtime(true);
	$stats[$resolver]['diff'] = $stats[$resolver]['end'] - $stats[$resolver]['start'];
}

//Order stats in desceding order of the "diff" value
uasort($stats, function ($a, $b) {
	return $a['diff'] - $b['diff'];
});

echo "\nSUDOKU Resolver challenge\n";
echo "-------------------------";
echo "\n\nHere is the benchmark result:\n";
$count = 1;
foreach ($stats as $stat) {
	echo $count . ". " . $stat['resolver'] . " - " . round($stat['diff'], 5) . "s \n";
	$count++;
}
